import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from './dto/create.user.dto';
import { UserService } from './user.service';
import { CommentService } from 'src/comment/comment.service';

@Controller('users')
export class UserController {
  constructor(
    private readonly services: UserService,
    private readonly commentServices: CommentService,
  ) {}

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.services.findOne(id);
  }

  @Post()
  create(@Body() payload: CreateUserDto) {
    return this.services.create(payload);
  }

  @Get(':id/comments')
  findUsersComments(@Param('id') id: string) {
    return this.commentServices.getUserComments(id);
  }

  @Patch(':id')
  update(@Param('id') id: number, @Body() payload: UpdateUserDto) {
    return this.services.update(id, payload);
  }
}
