import { Injectable } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from './dto/create.user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) {}

  async findOne(id: number) {
    return await this.userRepo.findOne({ where: { id: id } });
  }

  async create(payload: CreateUserDto) {
    const user = await this.userRepo.create(payload);
    return await this.userRepo.save(user);
  }

  async update(id: number, payload: UpdateUserDto) {
    return await this.userRepo.update(id, payload);
  }
}
