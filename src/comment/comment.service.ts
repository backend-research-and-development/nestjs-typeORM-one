import { Injectable } from '@nestjs/common';
import { CreateCommentDto } from './dto/create.comment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from 'src/entities/comment.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepo: Repository<Comment>,
  ) {}

  getUserComments(userId: string) {
    return { userId, data: [] };
  }

  async create(payload: CreateCommentDto) {
    const comment = await this.commentRepo.create(payload);
    return await this.commentRepo.save(comment);
  }
}
