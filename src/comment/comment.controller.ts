import { Body, Controller, Post } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create.comment.dto';

@Controller('comments')
export class CommentController {
  constructor(private readonly commentServices: CommentService) {}

  @Post()
  create(@Body() payload: CreateCommentDto) {
    return this.commentServices.create(payload);
  }
}
