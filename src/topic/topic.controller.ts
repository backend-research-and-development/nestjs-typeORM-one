import { Body, Controller, Get, Post } from '@nestjs/common';
import { TopicService } from './topic.service';
import { CreateTopicDTO } from './topic.dto';

@Controller('topics')
export class TopicController {
  constructor(private readonly topicServices: TopicService) {}

  @Post()
  create(@Body() payload: CreateTopicDTO) {
    return this.topicServices.create(payload);
  }

  @Get()
  getAllTopic() {
    return this.topicServices.getAllTopic();
  }
}
