import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Topic } from 'src/entities/topic.entity';
import { Repository } from 'typeorm';
import { CreateTopicDTO } from './topic.dto';

@Injectable()
export class TopicService {
  constructor(
    @InjectRepository(Topic) private readonly topicRepo: Repository<Topic>,
  ) {}

  async create(payload: CreateTopicDTO) {
    const topic = await this.topicRepo.create(payload);
    return await this.topicRepo.save(topic);
  }

  async getAllTopic() {
    return await this.topicRepo.find();
  }
}
