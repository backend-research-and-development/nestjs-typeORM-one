/* eslint-disable prettier/prettier */
import { Comment } from 'src/entities/comment.entity';
import { Topic } from 'src/entities/topic.entity';
import { User } from 'src/entities/user.entity';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

const config: PostgresConnectionOptions = {
  type: 'postgres',
  database: 'testdb',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: '753159',
  entities: [User, Comment, Topic],
  synchronize: true,
};

export default config;
